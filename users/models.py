from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class CustomUser(AbstractUser):
    telefono = models.CharField(max_length=64, blank=True)
    email_alt = models.CharField(max_length=64, blank=True)
    provincia = models.ForeignKey('Provincia', 
                                  on_delete=models.SET_NULL, 
                                  blank=True, null=True)
    
    class Meta:
        db_table = 'auth_user'


class Provincia(models.Model):
    nombre = models.CharField(max_length=64, blank=True)
    
    def __str__(self):
        return self.nombre
