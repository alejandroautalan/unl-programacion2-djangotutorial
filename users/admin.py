from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser, Provincia


class CustomUserAdmin(UserAdmin):
    model = CustomUser

adicionales = (
    ('Campos Adicionales',
     {'fields': ['telefono', 'email_alt', 'provincia']}),
)
CustomUserAdmin.fieldsets += adicionales

admin.site.register(CustomUser, CustomUserAdmin)

admin.site.register(Provincia)